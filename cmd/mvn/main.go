package main

import (
	"flag"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/mvn-settings-gen/pkg"
	"gitlab.com/av1o/mvn-settings-gen/pkg/maven"
	"gitlab.com/av1o/mvn-settings-gen/pkg/util"
	"os"
)

func main() {
	// setup logging
	log.SetOutput(os.Stderr)

	// system flags
	debug := flag.Bool("debug", false, "enables debug logging")

	// maven flags
	localRepository := flag.String("local-repository", "${user.home}/.m2/repository", "path of the build system's local repository")
	interactiveMode := flag.Bool("interactive-mode", false, "if Maven should attempt to interact with the user for input")
	offlineMode := flag.Bool("offline", false, "if this build system should operate in offline mode")

	var servers util.ArrayFlags
	var mirrors util.ArrayFlags
	var repos util.ArrayFlags

	flag.Var(&servers, "server", "server string in the following format type,id,username,password (e.g. default=my-maven-repo=joe.bloggs=hunter2)")
	flag.Var(&mirrors, "mirror", "mirror string in the following format id,name,url,of (e.g. central=Maven Central=https://repo1.maven.org/maven2=central)")
	flag.Var(&repos, "repo", "repo string in the following format id=name=url=releases=snapshots")

	flag.Parse()

	if *debug {
		log.SetLevel(log.DebugLevel)
		log.Debugf("enabled debug logging")
	} else {
		log.SetLevel(log.InfoLevel)
	}

	log.Infof("generating settings.xml using mvn-settings-gen %s-%s (%s)", pkg.Version, pkg.GitCommit, pkg.BuildDate)
	log.Debugf("os args: %+v", os.Args)

	// create the settings file
	settings := maven.NewSettings()

	// configure the settings
	settings.SimpleConfigurer(*localRepository, *interactiveMode, *offlineMode)
	settings.ServerConfigurer(servers)
	settings.MirrorConfigurer(mirrors)
	settings.RepoConfigurer(repos)

	log.Infof("generation complete, writing to stdout")

	str, err := settings.ToString()
	if err != nil {
		panic(err)
	}
	fmt.Print(str)
}
