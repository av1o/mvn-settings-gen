package main

import (
	_ "embed"
	"github.com/stretchr/testify/assert"
	"github.com/zenizh/go-capturer"
	"os"
	"testing"
)

//go:embed testdata/sample_0.xml
var sampleZero string

////go:embed testdata/sample_1.xml
//var sampleOne string

func TestServerRepo(t *testing.T) {
	os.Args = []string{"", "-server=default=nexus=joe.bloggs=password1", "-repo=nexus=Nexus=https://nexus.example.org=true=true", "-mirror=prism=Prism=https://prism.dcas.dev/maven=central"}
	out := capturer.CaptureStdout(main)
	assert.Equal(t, sampleZero, out)
}

//func TestGitLabCI(t *testing.T) {
//	assert.NoError(t, os.Setenv("CI_JOB_TOKEN", "hunter2"))
//	defer os.Unsetenv("CI_JOB_TOKEN")
//
//	os.Args = []string{"", "-server=gitlab-ci=gitlab=="}
//	out := capturer.CaptureStdout(main)
//	assert.Equal(t, sampleOne, out)
//}
