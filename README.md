# mvn-settings-gen - Generate a settings.xml on the fly!

[![pipeline status](https://gitlab.com/av1o/mvn-settings-gen/badges/master/pipeline.svg)](https://gitlab.com/av1o/mvn-settings-gen/-/commits/master)
[![coverage report](https://gitlab.com/av1o/mvn-settings-gen/badges/master/coverage.svg)](https://gitlab.com/av1o/mvn-settings-gen/-/commits/master)

mvn-settings-gen is a tool for generating a `settings.xml` file dynamically.

It is designed for use in a CI environment where passing around an immutable xml file is difficult.
This tool currently supports the following parts of the Maven settings schema:
* Servers
* Mirrors
* Profiles (sorta)
* Repositories

*mvn-settings-gen creates a default profile which it puts all repositories under*

### Usage

This tool can be used on its own, or as part of a CI job.

#### Standalone

```shell script
./mvn-settings-gen # prints minimal settings.xml
./mvn-settings-gen > ~/.m2/settings.xml
```

#### CI

GitLab CI (see extended example [here](https://gitlab.com/av1o/gitlab-ci-templates/-/blob/master/test/Maven-Test.gitlab-ci.yml))

```yaml
variables:
  AUTO_DEVOPS_MAVEN_SETTINGS_ARGS: '-mirror=prism=Prism=https://prism.dcas.dev/maven=central'

stages:
  - test

test_maven:
  stage: test
  image: registry.gitlab.com/av1o/base-images/maven:16-v3.6.3 # this image packages the binary
  dependencies: []
  before_script:
    - mvn --version
  script:
    - /opt/mvn-settings-gen ${AUTO_DEVOPS_MAVEN_SETTINGS_ARGS} > ci_settings.xml
    - mvn --settings ci_settings.xml --batch-mode test
  rules:
    - if: '$CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null'
      when: never
    - when: on_success
```


### CLI flags

#### -local-repository

Path of the local Maven repository. Defaults to `${user.home}/.m2/repository`

#### -interactive-mode

Whether Maven should attempt to interact with the user for input. This should almost always be `false`.

#### -offline

Whether Maven should run offline. Defaults to `false`

#### -server

This flag allows you to pass in maven servers.
```bash
type=id=username=password
default=my-repo=joe.bloggs=hunter2

# gitlab personal access token
gitlab=my-gitlab==password1

# gitlab deploy token
gitlab-deploy=my-gitlab==foobar
gitlab-deploy=my-gitlab== # from the CI_DEPLOY_PASSWORD env

# gitlab ci job
gitlab-ci=my-gitlab== # from the CI_JOB_TOKEN env
```

The server flag has some special support for GitLab Maven authentication via specific `type` values:

* `gitlab` - injects the password into the `Private-Token` HTTP header
* `gitlab-deploy` - injects the password into the `Deploy-Token` HTTP header and can be sourced from the `CI_DEPLOY_PASSWORD` environment variable if no value is provided.
* `gitlab-ci`/`gitlab2` - injects the password into the `Job-Token` HTTP header and can be sourced from the `CI_JOB_TOKEN` environment variable if no value is provided.

#### -mirror

This flag allows you to pass in maven mirrors.
```
id=name=url=of
central,Maven Central,https://repo1.maven.org/maven2=central
```

#### -repo

This flag allows you to pass in maven repositories.
```
id=name=url=releases=snapshots
my-repo=https://myrepo.example.org=true=true
```