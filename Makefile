PKG=gitlab.com/av1o/mvn-settings-gen
VERSION=$(shell cat version.txt)
GIT_COMMIT?=$(shell git rev-parse HEAD)
BUILD_DATE?=$(shell date -u +"%Y-%m-%dT%H:%M:%SZ")
LDFLAGS?="-X ${PKG}/pkg.Version=${VERSION} \
	-X ${PKG}/pkg.GitCommit=${GIT_COMMIT} \
	-X ${PKG}/pkg.BuildDate=${BUILD_DATE}"

.PHONY: bin/mvn-settings-gen
bin/mvn-settings-gen:
	CGO_ENABLED=0 go build -ldflags ${LDFLAGS} -o bin/mvn-settings-gen ./cmd/...

run: bin/mvn-settings-gen
	bin/mvn-settings-gen