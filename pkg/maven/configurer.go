package maven

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/mvn-settings-gen/pkg/maven/schema"
)

func (s *Settings) SimpleConfigurer(lr string, im bool, om bool) {
	s.LocalRepository = lr
	s.InteractiveMode = im
	s.Offline = om
}

func (s *Settings) ServerConfigurer(servers []string) {
	log.Infof("preparing to parse %d server strings", len(servers))
	for _, str := range servers {
		server, err := schema.NewServer(str)
		if err != nil {
			log.Error(err)
			continue
		}
		log.Infof("adding server %s", server.ID)
		// add the new server
		s.Servers.Servers = append(s.Servers.Servers, *server)
	}
}

func (s *Settings) MirrorConfigurer(mirrors []string) {
	log.Infof("preparing to parse %d mirror strings", len(mirrors))
	for _, str := range mirrors {
		mirror, err := schema.NewMirror(str)
		if err != nil {
			log.Error(err)
			continue
		}
		log.Infof("adding mirror %s", mirror.ID)
		// add the new mirror
		s.Mirrors.Mirrors = append(s.Mirrors.Mirrors, *mirror)
	}
}

func (s *Settings) RepoConfigurer(repos []string) {
	log.Infof("preparing to parse %d repo strings", len(repos))
	for _, str := range repos {
		repo, err := schema.NewRepo(str)
		if err != nil {
			log.Error(err)
			continue
		}
		log.Infof("adding repo %s", repo.ID)
		// add the new repo
		s.RequireProfile().Repositories.Repos = append(s.RequireProfile().Repositories.Repos, *repo)
	}
}
