package schema

import (
	"encoding/xml"
	"fmt"
	log "github.com/sirupsen/logrus"
	"os"
	"strings"
)

// NewServerList creates a new empty list of servers
func NewServerList() *ServerList {
	return &ServerList{
		Servers: []Server{},
	}
}

type ServerList struct {
	Servers []Server `xml:"servers"`
}

// https://maven.apache.org/settings.html#servers
type Server struct {
	XMLName       xml.Name      `xml:"server"`
	ID            string        `xml:"id"`
	Username      string        `xml:"username,omitempty"`
	Password      string        `xml:"password,omitempty"`
	Configuration *ServerConfig `xml:"configuration,omitempty"`
}

type ServerConfig struct {
	HTTPHeaders ServerConfigHeaders `xml:"httpHeaders,omitempty"`
}

type ServerConfigHeaders struct {
	XMLName  xml.Name             `xml:"httpHeaders"`
	Property ServerConfigProperty `xml:"property,omitempty"`
}

type ServerConfigProperty struct {
	XMLName xml.Name `xml:"property"`
	Name    string   `xml:"name,omitempty"`
	Value   string   `xml:"value,omitempty"`
}

// NewServer creates a new server from a given server-string
func NewServer(str string) (*Server, error) {
	// default,my-maven-server,joe.bloggs,password1
	// gitlab,gitlab,,password1 - gitlab uses a http header
	bits := strings.Split(str, "=")
	if len(bits) != 4 {
		return nil, fmt.Errorf("server string has incorrect chunked length (expected 4, got %d): %s", len(bits), str)
	}
	var server Server
	// check to see if the server needs to be created in a special way
	switch bits[0] {
	case TypeGitLab:
		server = newGitLabServer(bits[1], HeaderPrivate, bits[3])
	case TypeGitLabDeploy:
		server = newEnvServer(bits[1], HeaderDeploy, EnvDeployToken, bits[3])
	case "gitlab2":
		fallthrough
	case TypeGitLabJob:
		server = newEnvServer(bits[1], HeaderJob, EnvJobToken, bits[3])
	default:
		server = Server{
			ID:            bits[1],
			Username:      bits[2],
			Password:      bits[3],
			Configuration: nil,
		}
	}
	return &server, nil
}

// newGitLabServer creates a new server configured for use with the GitLab package registry
// and a dedicated token.
func newGitLabServer(id, header, password string) Server {
	log.Debugf("creating new gitlab-specific server %s with header (%s)", id, header)
	return newHeaderServer(id, header, password)
}

// newEnvServer creates a new server configured for use with the GitLab package registry
// from within a CI job.
func newEnvServer(id, header, env, token string) Server {
	if token == "" {
		log.Infof("fetching token from environment: %s", env)
		token = os.Getenv(env)
		if token == "" {
			log.Warningf("in env mode but no token or %s - this will probably fail", env)
		}
	}
	return newHeaderServer(id, header, token)
}

func newHeaderServer(id, header, token string) Server {
	return Server{
		ID:       id,
		Username: "",
		Password: "",
		Configuration: &ServerConfig{
			HTTPHeaders: ServerConfigHeaders{
				Property: ServerConfigProperty{
					Name:  header,
					Value: token,
				},
			},
		},
	}
}
