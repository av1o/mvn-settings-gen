package schema

import "net/http"

const (
	TypeGitLab       = "gitlab"
	TypeGitLabJob    = "gitlab-ci"
	TypeGitLabDeploy = "gitlab-deploy"
)

var (
	HeaderJob     = http.CanonicalHeaderKey("Job-Token")
	HeaderDeploy  = http.CanonicalHeaderKey("Deploy-Token")
	HeaderPrivate = http.CanonicalHeaderKey("Private-Token")
)

const (
	EnvJobToken    = "CI_JOB_TOKEN"       //nolint:gosec
	EnvDeployToken = "CI_DEPLOY_PASSWORD" //nolint:gosec
)
