package schema

import "encoding/xml"

type ProfileList struct {
	Profiles []Profile `xml:"profiles"`
}

// https://maven.apache.org/settings.html#profiles
type Profile struct {
	XMLName      xml.Name          `xml:"profile"`
	ID           string            `xml:"id"`
	Activation   ProfileActivation `xml:"activation"`
	Repositories RepoList          `xml:"repositories"`
}

// https://maven.apache.org/settings.html#activation
type ProfileActivation struct {
	ActiveByDefault bool `xml:"activeByDefault"`
}
