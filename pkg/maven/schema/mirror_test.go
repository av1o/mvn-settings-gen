package schema

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewEmptyMirror(t *testing.T) {
	str := ""
	_, err := NewMirror(str)
	assert.Error(t, err)
}

func TestNewBadMirror(t *testing.T) {
	str := "test=Something"
	_, err := NewMirror(str)
	assert.Error(t, err)
}

func TestNewMirror(t *testing.T) {
	str := "central=Maven Central=https://repo1.maven.org/maven2=central"
	mirror, err := NewMirror(str)
	assert.NoError(t, err)
	assert.Equal(t, "central", mirror.ID)
	assert.Equal(t, "Maven Central", mirror.Name)
	assert.Equal(t, "https://repo1.maven.org/maven2", mirror.URL)
	assert.Equal(t, "central", mirror.Of)
}
