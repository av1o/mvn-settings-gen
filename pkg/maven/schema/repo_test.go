package schema

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewEmptyRepo(t *testing.T) {
	str := ""
	_, err := NewRepo(str)
	assert.Error(t, err)
}

func TestNewBadRepo(t *testing.T) {
	str := "test=Something"
	_, err := NewRepo(str)
	assert.Error(t, err)
}

func TestNewReleasesRepo(t *testing.T) {
	str := "gitlab=GitLab=https://gitlab.com/api/v4/packages/maven=true=false"
	Repo, err := NewRepo(str)
	assert.NoError(t, err)
	assert.Equal(t, "gitlab", Repo.ID)
	assert.Equal(t, "GitLab", Repo.Name)
	assert.Equal(t, "https://gitlab.com/api/v4/packages/maven", Repo.URL)
	assert.True(t, Repo.Releases.Enabled)
	assert.False(t, Repo.Snapshots.Enabled)
}

func TestNewSnapshotsRepo(t *testing.T) {
	str := "gitlab=GitLab=https://gitlab.com/api/v4/packages/maven=false=true"
	Repo, err := NewRepo(str)
	assert.NoError(t, err)
	assert.Equal(t, "gitlab", Repo.ID)
	assert.Equal(t, "GitLab", Repo.Name)
	assert.Equal(t, "https://gitlab.com/api/v4/packages/maven", Repo.URL)
	assert.False(t, Repo.Releases.Enabled)
	assert.True(t, Repo.Snapshots.Enabled)
}

func TestNewBiRepo(t *testing.T) {
	str := "gitlab=GitLab=https://gitlab.com/api/v4/packages/maven=true=true"
	Repo, err := NewRepo(str)
	assert.NoError(t, err)
	assert.Equal(t, "gitlab", Repo.ID)
	assert.Equal(t, "GitLab", Repo.Name)
	assert.Equal(t, "https://gitlab.com/api/v4/packages/maven", Repo.URL)
	assert.True(t, Repo.Releases.Enabled)
	assert.True(t, Repo.Snapshots.Enabled)
}
