package schema

import (
	"encoding/xml"
	"fmt"
	log "github.com/sirupsen/logrus"
	"strconv"
	"strings"
)

type RepoList struct {
	Repos []Repo `xml:"repositories"`
}

// https://maven.apache.org/settings.html#repositories
type Repo struct {
	XMLName   xml.Name    `xml:"repository"`
	ID        string      `xml:"id"`
	Name      string      `xml:"name"`
	URL       string      `xml:"url"`
	Layout    string      `xml:"layout,omitempty"`
	Releases  *RepoPolicy `xml:"releases,omitempty"`
	Snapshots *RepoPolicy `xml:"snapshots,omitempty"`
}

type RepoPolicy struct {
	Enabled        bool   `xml:"enabled"`
	UpdatePolicy   string `xml:"updatePolicy"`
	ChecksumPolicy string `xml:"checksumPolicy"`
}

func NewRepo(str string) (*Repo, error) {
	// id,name,url,releases
	bits := strings.Split(str, "=")
	if len(bits) != 5 {
		return nil, fmt.Errorf("repo string has incorrect chunked length (expected 5, got %d): %s", len(bits), str)
	}
	// check whether this repo is for releases and/or snapshots
	releases, err := strconv.ParseBool(bits[3])
	if err != nil {
		return nil, fmt.Errorf("failed to parse boolean from %s", bits[3])
	}
	snapshots, err := strconv.ParseBool(bits[4])
	if err != nil {
		return nil, fmt.Errorf("failed to parse boolean from %s", bits[4])
	}
	if releases {
		log.Infof("creating new releases repo %s", bits[0])
	}
	if snapshots {
		log.Infof("creating new snapshots repo %s", bits[0])
	}
	return &Repo{
		ID:   bits[0],
		Name: bits[1],
		URL:  bits[2],
		Releases: &RepoPolicy{
			Enabled:        releases,
			UpdatePolicy:   "always",
			ChecksumPolicy: "warn",
		},
		Snapshots: &RepoPolicy{
			Enabled:        snapshots,
			UpdatePolicy:   "always",
			ChecksumPolicy: "warn",
		},
	}, nil
}
