package schema

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestNewEmptyServer(t *testing.T) {
	str := ""
	_, err := NewServer(str)
	assert.Error(t, err)
}

func TestNewBadServer(t *testing.T) {
	str := "test=Something"
	_, err := NewServer(str)
	assert.Error(t, err)
}

func TestNewDefaultServer(t *testing.T) {
	str := "default=my-maven-repo=joe.bloggs=hunter2"
	server, err := NewServer(str)
	assert.NoError(t, err)
	assert.Equal(t, "my-maven-repo", server.ID)
	assert.Equal(t, "joe.bloggs", server.Username)
	assert.Equal(t, "hunter2", server.Password)
	assert.Nil(t, server.Configuration)
}

func TestNewGitLabServer(t *testing.T) {
	str := "gitlab=my-maven-repo==hunter2"
	server, err := NewServer(str)
	assert.NoError(t, err)
	assert.Equal(t, "my-maven-repo", server.ID)
	assert.Empty(t, server.Username)
	assert.Empty(t, server.Password)
	assert.NotNil(t, server.Configuration)
	assert.Equal(t, "hunter2", server.Configuration.HTTPHeaders.Property.Value)
}

func TestNewGitLab2Server(t *testing.T) {
	assert.NoError(t, os.Setenv("CI_JOB_TOKEN", "hunter2"))
	defer os.Unsetenv("CI_JOB_TOKEN")

	str := "gitlab2=my-maven-repo==password"
	server, err := NewServer(str)
	assert.NoError(t, err)
	assert.Equal(t, "my-maven-repo", server.ID)
	assert.Empty(t, server.Username)
	assert.Empty(t, server.Password)
	assert.NotNil(t, server.Configuration)
	assert.Equal(t, "password", server.Configuration.HTTPHeaders.Property.Value)
}

func TestNewGitLab2ServerNoPassword(t *testing.T) {
	assert.NoError(t, os.Setenv("CI_JOB_TOKEN", "hunter2"))
	defer os.Unsetenv("CI_JOB_TOKEN")

	str := "gitlab-ci=my-maven-repo=="
	server, err := NewServer(str)
	assert.NoError(t, err)
	assert.Equal(t, "my-maven-repo", server.ID)
	assert.Empty(t, server.Username)
	assert.Empty(t, server.Password)
	assert.NotNil(t, server.Configuration)
	assert.Equal(t, "hunter2", server.Configuration.HTTPHeaders.Property.Value)
}

func TestNewHeaderServer(t *testing.T) {
	var cases = []struct {
		header string
		token  string
	}{
		{
			HeaderPrivate,
			"password",
		},
		{
			HeaderDeploy,
			"gitlab-deplyo-token",
		},
		{
			HeaderJob,
			"sometoken",
		},
	}

	for _, tt := range cases {
		t.Run(tt.header, func(t *testing.T) {
			srv := newHeaderServer("", tt.header, tt.token)
			assert.EqualValues(t, tt.header, srv.Configuration.HTTPHeaders.Property.Name)
			assert.EqualValues(t, tt.token, srv.Configuration.HTTPHeaders.Property.Value)
		})
	}
}
