package schema

import (
	"encoding/xml"
	"fmt"
	"strings"
)

// NewMirrorList creates a new empty list of mirrors
func NewMirrorList() *MirrorList {
	return &MirrorList{
		Mirrors: []Mirror{},
	}
}

type MirrorList struct {
	Mirrors []Mirror `xml:"mirrors"`
}

// https://maven.apache.org/settings.html#mirrors
type Mirror struct {
	XMLName xml.Name `xml:"mirror"`
	ID      string   `xml:"id"`
	Name    string   `xml:"name"`
	URL     string   `xml:"url"`
	Of      string   `xml:"mirrorOf"`
}

// NewMirror creates a new mirror from a given mirror-string
func NewMirror(str string) (*Mirror, error) {
	// central,Maven Central,https://repo1.maven.org/maven2,central
	bits := strings.Split(str, "=")
	if len(bits) != 4 {
		return nil, fmt.Errorf("mirror string has incorrect chunked length (expected 4, got %d): %s", len(bits), str)
	}
	return &Mirror{
		ID:   bits[0],
		Name: bits[1],
		URL:  bits[2],
		Of:   bits[3],
	}, nil
}
