package maven

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestServerConfigurer(t *testing.T) {
	settings := NewSettings()
	settings.ServerConfigurer([]string{"default=my-maven-repo=joe.bloggs=hunter2"})
	assert.Equal(t, 1, len(settings.Servers.Servers))
}

func TestMirrorConfigurer(t *testing.T) {
	settings := NewSettings()
	settings.MirrorConfigurer([]string{"central=Maven Central=https://repo1.maven.org/maven2=central"})
	assert.Equal(t, 1, len(settings.Mirrors.Mirrors))
}

func TestRepoConfigurer(t *testing.T) {
	settings := NewSettings()
	settings.RepoConfigurer([]string{"gitlab=GitLab=https://gitlab.com/api/v4/packages/maven=true=false"})
	assert.Equal(t, 1, len(settings.RequireProfile().Repositories.Repos))
}
