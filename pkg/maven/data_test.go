package maven

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewSettings(t *testing.T) {
	settings := NewSettings()
	data, err := settings.ToString()
	if err != nil {
		t.Error(err)
	}
	assert.NotZero(t, len(data))
}

func TestSettings_RequireProfile(t *testing.T) {
	settings := NewSettings()
	profile := settings.RequireProfile()
	assert.NotNil(t, profile)
	assert.True(t, profile.Activation.ActiveByDefault)
}
