package maven

import (
	"encoding/xml"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/mvn-settings-gen/pkg/maven/schema"
)

// https://maven.apache.org/settings.html#quick-overview
type Settings struct {
	XMLName xml.Name `xml:"http://maven.apache.org/SETTINGS/1.0.0 settings"`
	// xml front-matter
	XMLXsi  string `xml:"xmlns:xsi,attr,omitempty"`
	XmlnsSL string `xml:"xsi:schemaLocation,attr"`
	// actual properties
	LocalRepository string `xml:"localRepository"`
	InteractiveMode bool   `xml:"interactiveMode"`
	Offline         bool   `xml:"offline"`

	Servers  *schema.ServerList  `xml:"servers"`
	Mirrors  *schema.MirrorList  `xml:"mirrors"`
	Profiles *schema.ProfileList `xml:"profiles"`
}

// NewSettings creates an empty settings object with default values
func NewSettings() *Settings {
	s := new(Settings)
	s.XMLXsi = "http://www.w3.org/2001/XMLSchema-instance"
	s.XmlnsSL = "http://maven.apache.org/SETTINGS/1.0.0 https://maven.apache.org/xsd/settings-1.0.0.xsd"
	// set default values
	s.LocalRepository = "${user.home}/.m2/repository"
	s.InteractiveMode = false // assume we're in CI mode
	s.Offline = false

	s.Servers = schema.NewServerList()
	s.Mirrors = schema.NewMirrorList()
	// create the default profile
	s.Profiles = &schema.ProfileList{
		Profiles: []schema.Profile{
			{
				ID: "default",
				Activation: schema.ProfileActivation{
					ActiveByDefault: true,
				},
				Repositories: schema.RepoList{},
			},
		},
	}
	return s
}

// ToString converts a settings object into an XML string
func (s *Settings) ToString() (string, error) {
	data, err := xml.MarshalIndent(s, "", "\t")
	if err != nil {
		log.WithError(err).Error("failed to marshal xml")
		return "", err
	}
	return string(data), err
}

func (s *Settings) RequireProfile() *schema.Profile {
	if len(s.Profiles.Profiles) == 0 {
		return nil
	}
	return &s.Profiles.Profiles[0]
}
