package util

import "strings"

type ArrayFlags []string

func (af *ArrayFlags) String() string {
	return strings.Join(*af, ",")
}

func (af *ArrayFlags) Set(val string) error {
	*af = append(*af, strings.TrimSpace(val))
	return nil
}
