package util

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestArrayFlags_Set(t *testing.T) {
	flags := ArrayFlags{}
	_ = flags.Set("test")
	assert.Equal(t, 1, len(flags))
}

func TestArrayFlags_String(t *testing.T) {
	flags := ArrayFlags{
		"test",
		"test1",
	}
	str := flags.String()
	assert.Equal(t, "test,test1", str)
}
